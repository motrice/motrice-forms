import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from '../../utils/reduxForm';

class BtInput extends React.Component {

  render() {
  	const {fieldName, formSchemaField, uiSchemaField, field} = this.props; 

	let description = '';
	let help = '';
	let type = 'text';
	if (uiSchemaField !== undefined) {
		description = uiSchemaField['ui:description'] || '';
		help = uiSchemaField['ui:help'] || '';
		type = uiSchemaField['ui:widget'] || 'text';
		if (type === 'updown') {
			type = 'number';
		}
	}

	return (<div key={fieldName} className="form-group">
	  <label htmlFor={fieldName}>{formSchemaField.title}</label>
	  <input name={fieldName} type={type} className="form-control" id={fieldName} aria-describedby={fieldName + 'Help'} placeholder={description} {...field}/>
	  <small id={fieldName + 'Help'} className="form-text text-muted">{help}</small>
	</div>);
  }
}

export default (BtInput);