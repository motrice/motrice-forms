import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from '../../utils/reduxForm';
import BtInput from '../bootstrap/BtInput';

class MoForm extends React.Component {

  render() {
    const { formSchema, uiSchema } = this.props.formConfig;
    const fields = this.props.fields;
    return (
      <form>
        <div>
          <fieldset>
            <legend>{formSchema.title}</legend>
            <p>{formSchema.description}</p> 
            {
              Object.keys(fields).map((field) => {
                return (
                  <BtInput 
                    fieldName={field} field={fields[field]} 
                    formSchemaField={formSchema.properties[field]} 
                    uiSchemaField={uiSchema[field]}
                    />
                  );
              })
            }
        </fieldset>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state, config) => {
  return {
    form: state.formReducer.form
   };
}

const enhancedForm = reduxForm() (MoForm);

export default connect(mapStateToProps) (enhancedForm);