	

const formSchema = {
  "title": "A registration form",
  "description": "A simple form example.",
  "type": "object",
  "required": [
    "firstName",
    "lastName"
  ],
  "properties": {
    "firstName": {
      "type": "string",
      "title": "First name"
    },
    "lastName": {
      "type": "string",
      "title": "Last name"
    },
    "age": {
      "type": "integer",
      "title": "Age"
    },
    "bio": {
      "type": "string",
      "title": "Bio"
    },
    "date": {
      "type": "string",
      "title": "Date"
    },
    "password": {
      "type": "string",
      "title": "Password",
      "minLength": 3
    },
    "telephone": {
      "type": "string",
      "title": "Telephone",
      "minLength": 10
    }
  }
};

const uiSchema = {
  "firstName": {
    "ui:autofocus": true,
    "ui:emptyValue": "",
     "ui:description": "Förnamn"
  },
  "age": {
    "ui:widget": "updown",
    "ui:title": "Age of person",
    "ui:description": "(earthian year)"
  },
  "bio": {
    "ui:widget": "textarea",
    "ui:description": "bioblabladesc",
    "ui:help": "Hint: Make it stronger!"
  },
  "password": {
    "ui:widget": "password",
    "ui:help": "Hint: Make it strong!"
  },
  "date": {
    "ui:widget": "alt-datetime"
  },
  "telephone": {
    "ui:options": {
      "inputType": "tel"
    }
  }
};

const data = {
  "firstName": "Chuck",
  "lastName": "Norris",
  "age": 75,
  "bio": "Roundhouse kicking asses since 1940\nasddsadsadsadsad\nsdsadsad",
  "date": "2017-12-11T23:26:08.000Z",
  "password": "noneed"
};
 
const form = 'my-form';

export default {
  formSchema: formSchema,
  uiSchema: uiSchema,
  initialData: data,
  form: form
};



