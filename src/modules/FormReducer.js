import { bindRedux} from '../utils/reduxForm';
import formConfig from '../utils/formConfig';

const { state: formState, reducer: formReducer } = bindRedux(formConfig);

const initialState = {
  ...formState
};

export default (state = initialState, action) => {

  console.log('REDUCER ', state, ' action ', action);
  switch (action.type) {
    case 'SOME_ACTION': {
      let newState = state;

      if (newState[action.form] === undefined) {
        newState[action.form] = {[action.field] : action.newValue}; 
      }
      else {
        newState[action.form][action.field] = action.newValue;
      }
      console.log('SET FORM newState', newState);

      return {
        newState,
        ...state
      };
    }

    default:
      return formReducer(state, action);
  }
}
