import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import formReducer from './FormReducer'


export default combineReducers({
  routing: routerReducer,
  formReducer
})
